<?php

$app->group([
    'prefix' => '/character/{character:[A-Za-z0-9_\-.]{4,10}}',
    'namespace' => 'Character'
], function () use ($app) {

    $app->get('/', 'Character@index');
    $app->get('/inventory', 'Inventory@index');

    $app->group([
        'middleware' => 'auth',
    ], function () use ($app) {
        $app->post('/reset', 'Reset@index');
        $app->post('/grandreset', 'GrandReset@index');
        $app->post('/pkclear', 'PkClear@index');
        $app->post('/addstats', 'AddStats@index');
    });

});

$app->group([
    'prefix' => '/user/{username:[A-Za-z0-9_\-.]{4,10}}',
    'middleware' => 'auth',
    'namespace' => 'User'
], function () use ($app) {

    $app->get('/', 'User@index');
    $app->get('/warehouse', 'Warehouse@index');
    $app->get('/characters', 'Characters@index');
    $app->post('/edit/password', 'Edit@password');
    $app->post('/edit/email', 'Edit@email');

});

$app->group([
    'prefix' => '/guild/{guild:[A-Za-z0-9_\-.]{4,10}}',
    'namespace' => 'Guild'
], function () use ($app) {

    $app->get('/', 'Guild@index');
    $app->get('/members', 'Members@index');

});

$app->group([
    'prefix' => '/auth',
    'namespace' => 'Auth'
], function () use ($app) {

    $app->post('/login', 'Login@index');
    $app->put('/register', 'Register@index');

});

// FIXME: Debug routes, to be removed!
$app->group([
    'prefix' => '/item/{hex:(?:0x)?[A-Fa-f0-9]+}'
], function () use ($app) {

    $app->get('/decode', function ($hex) {
        $item = new App\Models\Inventory\Item($hex);
        return $item->toJson();
    });

});
