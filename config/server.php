<?php

return [
    'md5' => false,
    'version' => 1, // 0 - Season I and below; 1 - Season 2 to Season 5 (inclusive)
    'maxstats' => 32767,
    'reset' => [
        'max' => 100,
        'level' => 400,
        'points' => 500,
        'cost' => 1000000
    ],
    'pkclear' => [
        'cost' => 1000000,
        'perkill' => true
    ],
    'addstats' => [
        'cost' => 1000000
    ]
];
