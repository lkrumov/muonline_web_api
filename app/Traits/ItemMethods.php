<?php

namespace App\Traits;

use App\Enums\Miscellaneous;
use Illuminate\Support\Facades\DB;

trait ItemMethods {
    public function getRefineryOptions($group) {
        switch ($group) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return [
                    'Additional Damage +200',
                    'Pow Success Rate +10%'
                ];
            case 7:
                return [
                    'SD Recovery Rate +20%',
                    'Defense Success Rate +10%'
                ];
            case 8:
                return [
                    'SD Auto Recovery',
                    'Defense Success Rate +10%'
                ];
            case 9:
            case 10:
                return [
                    'Defense Skill +200',
                    'Defense Success Rate +10%'
                ];
            case 11:
                return [
                    'Max HP',
                    'Defense Success Rate +10%'
                ];
            default:
                throw new \Exception('Unable to decode refinery option.', 422);
        }
    }

    public function getJewelOfLifeOption($group, $level) {
        $option = ['sign' => '+'];

        if ($group === 13) {
            $option['value'] = $level;
            $option['text'] = 'Automatic HP Recovery rate';
            $option['sign'] = '%';
        } else if ($group === 6) {
            $option['value'] = $level * 5;
            $option['text'] = 'Additional Defense rate';
        } else {
            $option['value'] = $level * 4;
            $option['text'] = 'Additional Damage';
        }

        return $option;
    }

    public function getSockets($socketHex) {
        $socketOptionsText = [];

        for ($slot = 0; $slot < 5; $slot++) {
            $offset = $slot * 2;
            $socketOption = $this->subdec($offset, 2, $socketHex);

            if ($socketOption === 0) {
                break;
            }

            if ($socketOption === 255) {
                $socketOptionsText[] = ['empty' => true];
                continue;
            }

            $socketLevel = $this->getSocketLevel($socketOption);
            $socket = DB::table('DPWebShop_Socket')
                        ->select([
                            'socket_option as option',
                            'socket_type as type',
                            "lvl_{$socketLevel} as level"
                        ])
                        ->where('socket_id', $socketOption)
                        ->first();

            $socketOptionsText[] = [
                'type' => $this->getSocketType($socket->type),
                'option' => trim(str_replace('%', '', $socket->option)),
                'level' => $socket->level,
                'sign' => strpos($socket->option, '%') !== false ? '%' : '+'
            ];
        }

        return $socketOptionsText;
    }

    public function getSocketType($type) {
        switch ($type) {
            case 1:
                return 'Fire';
            case 2:
                return 'Water';
            case 3:
                return 'Ice';
            case 4:
                return 'Wind';
            case 5:
                return 'Lightning';
            case 6:
                return 'Ground';
            default:
                throw new \Exception('Undefined socket type.', 422);
        }
    }

    public function getSocketLevel(&$socketOption) {
        $socketLevel = 1;

        while ($socketOption > 50) {
            $socketOption -= 50;
            $socketLevel++;
        }

        return $socketLevel;
    }

    public function getHarmonyOption($hex, $type) {
        $id = $this->subdec(0, 1, $hex);
        $level = $this->subdec(1, 1, $hex);
        $harmonyText = [];

        $harmony = DB::table('DPWebShop_Harmony')
                        ->select(
                            'harmony_option as option',
                            "lvl_{$level} as level"
                        )
                        ->where([
                            ['harmony_id', '=', $id],
                            ['harmony_type', '=', $type]
                        ])
                        ->first();

        return [
            'option' => trim(str_replace('%', '', $harmony->option)),
            'level' => $harmony->level,
            'sign' => strpos($harmony->option, '%') !== false ? '%' : '+'
        ];
    }

    public function getFenrirOptions($availableOptions) {
        $skill = 'Plasma storm skill (Mana: 50)';
        $options = [];
        $type = '';

        if ($availableOptions[2]) {
            $type = 'Defense';
            $options = [
                'Absorb final damage 10%'
            ];
        } else if ($availableOptions[3]) {
            $type = 'Golden';
        } else {
            $type = 'Destroy';
            $options = [
                'Increase final damage 10%',
            ];
        }

        $options[] = 'Increase speed';

        return [
            'skill' => $skill,
            'options' => $options,
            'type' => $type
        ];
    }

    public function calculateExcellentOptions(&$excellentOptions, $textSet, &$availableOptions) {
        $optionsText = [];

        for ($option = 5; $option >= 0; $option--) {
            $optionCost = pow(2, $option);

            if ($excellentOptions < $optionCost) {
                continue;
            }

            $optionText = $textSet[$option];
            $excellentOptions -= $optionCost;
            $availableOptions[$option] = true;

            if ($optionText !== Miscellaneous::NO_EFFECT) {
                $optionsText[] = $optionText;
            }
        }

        return $optionsText;
    }

    // TODO: Generate better image url. Fix the filenames
    public function getItemImage($id, $group, $type, $level) {
        $group = sprintf('%02X', $group);
        $id = sprintf('%02X', $id);

        if ($level < 10) {
            $level = "0{$level}";
        }

        return '00' . $group . $id . $level;
    }

    protected function subdec($start, $end, $hex = null) {
        $hex = $this->substr($start, $end, $hex);

        return hexdec($hex);
    }

    protected function substr($start, $end, $hex = null) {
        $hex = $hex === null ? $this->hex : $hex;

        return substr($hex, $start, $end);
    }
}
