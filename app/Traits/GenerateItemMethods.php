<?php

namespace App\Traits;

use App\Enums\Miscellaneous;
use Illuminate\Support\Facades\DB;

trait GenerateItemMethods {

    public function generateId($id) {
        return $this->hex($id);
    }

    public function generateDurabillity($durability) {
        return $this->hex($durability);
    }

    public function generateGroup($group) {
        return $this->hex($group);
    }

    public function generateBasicOptions($level, $jolOption, $skill, $luck) {
        $basicOptionsDec = 0;

        //Item level
        $basicOptionsDec += $level * 8;

        //Item Option
        $basicOptionsDec += $jolOption < 4 ? $jolOption : ($jolOption - 4);

        //Skill
        !$skill ?: $basicOptionsDec += 128;

        //Luck
        !$luck ?: $basicOptionsDec += 4;

        return $this->hex($basicOptionsDec);
    }

    public function generateExcellentOptions(array $excellentOptions, $jolOption) {
        $excellentOptionsDec = $jolOption >= 4 ? 64 : 0;

        foreach ($excellentOptions as $index => $option) {
            !$option ?: $excellentOptionsDec += pow(2, $index);
        }

        return $this->hex($excellentOptionsDec);
    }

    public function generateSocketOptions($socketOptions) {
        $socketsHex = '';

        foreach ($socketOptions as $socket) {
            if ($socket['type'] === 'empty') {
                $socketsHex .= 'FF';
                continue;
            }

            $socket = ($socket['level'] - 1) * 50;
            $socket += $socket['id'];

            $socketsHex .= $this->hex($socket);
        }

        return str_pad($socketsHex, 10, '0');
    }

    public function generateHarmonyOption($harmony) {
        return $this->hex($harmony['id'], 1) . $this->hex($harmony['level'], 1);
    }

    public function generateRefineryOptions($refinery) {
        // Easy, right? :)
        return $refinery ? $this->hex(8, 1) : '0';
    }

    public function generateAncientOption($level) {
        return $this->hex($level * 5);
    }

    public function generateFenrirOption($type) {
        switch($type) {
            case Miscellaneous::FENRIR_DEMAGE:
                return $this->hex(1);
            case Miscellaneous::FENRIR_DEFENSE:
                return $this->hex(2);
            case Miscellaneous::FENRIR_ILLUSION:
                return $this->hex(4);
        }
    }

    public function generateSerial() {
        DB::statement('exec WZ_GetItemSerial');

        $gameServer = DB::table('GameServerInfo')
                        ->select('ItemCount')
                        ->first();

        $serialHex = $this->hex($gameServer->ItemCount, 8);

        return $serialHex;
    }

    private function hex($dec, $bits = 2) {
        $bits = $bits < 10 ? '0' . $bits : $bits;

        return sprintf("%{$bits}X", $dec);
    }
}
