<?php

namespace App\Factories;

use App\Models\Item\Decode as DecodeSeason2To5;
use App\Models\Item\DecodeOldVersions as DecodeSeason1AndBelow;

class ItemDecode {
    public static function create($hex) {
        switch(config('server.version')) {
            case 0:
                return new DecodeSeason1AndBelow($hex);
            default:
                return new DecodeSeason2To5($hex);
        }
    }
}
