<?php

namespace App\Factories;

use App\Models\Item\Generate as GenerateSeason2To5;
use App\Models\Item\GenerateOldVersions as GenerateSeason1AndBelow;

class ItemGenerator {
    public static function create(array $options) {
        switch(config('server.version')) {
            case 0:
                return new GenerateSeason1AndBelow($options);
            default:
                return new GenerateSeason2To5($options);
        }
    }
}
