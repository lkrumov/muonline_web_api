<?php

namespace App\Interfaces;

interface IJsonable {
    public function toJson();
}
