<?php

namespace App\Models;

use App\Models\Base as Model;
use App\Factories\ItemDecode as Item;
use App\Interfaces\IItemGenerator;

use Illuminate\Support\Facades\DB;

abstract class ItemStorage extends Model {
    protected $itemColumn = 'Items';
    protected $defaultOffset = null;

    private $slots;
    private $width = 8;
    private $emptyItem;
    private $takenSlots;
    private $itemData;
    public $hexLength;

    public function addItem(IItemGenerator $item, $slot = null) {
        if ($slot !== null && !$this->isSlotTaken($slot, $item->get('x'), $item->get('y'))) {
            // You must be a tea pot if you got yourself here :)
            throw new Exception('The slot given to addItem is taken.', 418);
        }

        if ($slot === null) {
            $slot = $this->getFirstFreeSlotFor($item->get('x'), $item->get('y'));

            if ($slot === -1) {
                throw new Exception('Couldn\'t find a free slot.', 507);
            }
        }

        $items = $this->get($this->itemColumn);
        $offset = $this->hexLength * $slot;
        $newItems = substr_replace($items, $item->getHex(), $offset, $this->hexLength);

        $this->set($this->itemColumn, $newItems);

        return $this->save();
    }

    public function removeItem($slot) {
        $slots = $this->getTakenSlots();

        if (!$slots[$slot]) {
            throw new Exception('You\'re trying to remove item from an empty slot.', 204);
        }

        $items = $this->get($this->itemColumn);
        $offset = $this->hexLength * $slot;
        $newItems = substr_replace($items, $this->emptyItem, $offset, $this->hexLength);

        $this->set($this->itemColumn, $newItems);

        return $this->save();
    }

    public function getFreeSlotsFor($x, $y) {
        $freeSlots = [];

        for ($slot = 0; $slot < $this->slots; $slot++) {
            $freeSlot = $this->getFirstFreeSlotFor($x, $y, $slot);

            if ($freeSlot === -1) {
                break;
            }

            $freeSlots[] = $freeSlot;
            $slot = $freeSlot;
        }

        return $freeSlots;
    }

    public function getFirstFreeSlotFor($x, $y, $offset = null) {
        // In order to get the first available free slot,
        // we need to know the taken slots, right? :)
        $slots = $this->getTakenSlots();
        $freeSlot = -1;

        // Set the offset. We have 3 options here:
        // 1. If the DO* is null - set the passed value
        // 2. The passed offset is null or it's smaller than the DO - set DO then
        // 3. The passed offset is bigger than the DO - set the passed value
        // * DO - $this->defaultOffset
        $offset = $this->defaultOffset === null ? $offset :
                    $offset === null || $offset < $this->defaultOffset ?
                        $this->defaultOffset : $offset;

        foreach ($slots as $slot => $occupied) {
            if ($occupied || ($offset !== null && $slot < $offset)) {
                continue;
            }

            $slotsTaken = false;

            for ($slotY = 0; $slotY < $y; $slotY++) {
                $itemStart = $slot + ($this->width * $slotY);
                $startRow = floor($itemStart / $this->width);
                $itemEnd = $itemStart + $x;

                for ($slotX = $itemStart; $slotX < $itemEnd; $slotX++) {
                    $currentRow = floor($slotX / $this->width);

                    if (!isset($slots[$slotX]) || $slots[$slotX] || $currentRow !== $startRow) {
                        $slotsTaken = true;
                    }
                }
            }

            if (!$slotsTaken) {
                $freeSlot = $slot;
                break;
            }
        }

        return $freeSlot;
    }

    public function isSlotTaken($slot, $x, $y) {
        $slots = $this->getTakenSlots();

        for ($slotY = 0; $slotY < $y; $slotY++) {
            $itemStart = $slot + ($this->width * $slotY);
            $startRow = floor($itemStart / $this->width);
            $itemEnd = $itemStart + $x;

            for ($slotX = $itemStart; $slotX < $itemEnd; $slotX++) {
                $currentRow = floor($slotX / $this->width);

                if (!isset($slots[$slotX]) || $slots[$slotX] || $currentRow !== $startRow) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getTakenSlots() {
        if ($this->takenSlots !== null) {
            return $this->takenSlots;
        }

        $slots = array_fill(0, $this->slots, false);
        $items = $this->getItems();

        foreach ($items as $slot => $item) {
            if (isset($item['empty'])) {
                continue;
            }

            for ($slotY = 0; $slotY < $item['dimensions']['y']; $slotY++) {
                $itemStart = $slot + ($this->width * $slotY);
                $itemEnd = $itemStart + $item['dimensions']['x'];

                for ($slotX = $itemStart; $slotX < $itemEnd; $slotX++) {
                    $slots[$slotX] = true;
                }
            }
        }

        $this->takenSlots = $slots;

        return $slots;
    }

    public function getItems() {
        if ($this->itemData !== null) {
            return $this->itemData;
        }

        $items = $this->get($this->itemColumn);
        $itemData = [];

        for ($i = 0; $i < $this->slots; $i++) {
            $itemHex = substr($items, ($i * $this->hexLength), $this->hexLength);

            if ($itemHex !== $this->emptyItem) {
                $itemData[] = Item::create($itemHex)->toJson();
            } else {
                $itemData[] = [
                    'empty' => true
                ];
            }
        }

        $this->itemData = $itemData;

        return $this->itemData;
    }

    protected function setHexLength($length) {
        $this->hexLength = $length;
        $this->emptyItem = str_repeat('F', $length);
    }

    protected function setSlotsCount($count) {
        $this->slots = $count;
    }

    protected function setSlotsWidth($width) {
        $this->width = $width;
    }
}
