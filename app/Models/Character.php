<?php

namespace App\Models;

use App\Enums\CharacterClass;
use App\Models\Base as Model;

use Illuminate\Support\Facades\DB;

class Character extends Model {
    protected $varbinaryFields = [
        'Inventory',
        'MagicList',
        'Quest'
    ];

    public function __construct($name) {
        $isConnected = DB::raw('CASE WHEN AccountCharacter.GameIDC=Character.Name THEN 1 ELSE 0 END AS Connected');
        $character = DB::table('Character')
                        ->leftJoin('AccountCharacter', 'AccountCharacter.Id', '=', 'Character.AccountID')
                        ->select([
                            'Character.*',
                            $isConnected
                        ])->where('Character.Name', $name);

        if ($character->count() !== 1) {
            throw new \Exception('Character not found.', 404);
        }

        $this->setSource($character);
        $this->setData($character->first());
    }

    public function isLeadershipEligible() {
        return CharacterClass::isLord($this->get('Class'));
    }
}
