<?php

namespace App\Models;

use App\Models\Base as Model;

use Illuminate\Support\Facades\DB;

class User extends Model {
    public function __construct($username) {
        if (config('server.md5')) {
            $this->varbinaryFields = [
                'memb__pwd'
            ];
        }

        $user = DB::table('MEMB_INFO')
                    ->leftJoin('AccountCharacter', 'AccountCharacter.id', '=', 'MEMB_INFO.memb___id')
                    ->leftJoin('MEMB_STAT', 'MEMB_STAT.memb___id', '=', 'MEMB_INFO.memb___id')
                    ->where('MEMB_INFO.memb___id', $username);

        if ($user->count() !== 1) {
            throw new \Exception('User not found.', 404);
        }

        $this->setSource($user);
        $this->setData($user->first());
    }

    public function hasCharacter($character) {
        return in_array($character, $this->getMany([
            'GameID1', 'GameID2', 'GameID3',
            'GameID4', 'GameID5'
        ]));
    }
}
