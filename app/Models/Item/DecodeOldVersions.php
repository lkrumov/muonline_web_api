<?php

namespace App\Models\Item;

use App\Interfaces\IDecodable;
use App\Interfaces\IJsonable;

class DecodeOldVersions extends BaseDecode implements IJsonable {
    protected $hexLength = 20;
    protected $harmonyOption;
    protected $socketOptions;

    // Renderable values
    public $socketOptionsText = [];
    public $harmonyOptionText = [];

    protected function decode() {
        // Item's basic information
        $this->setItemIdAndGroup();
        $this->durability = $this->subdec(4, 2);         // Durability
        $this->serial = $this->substr(6, 8);             // Serial Number

        // Item's options
        $this->basicOptions = $this->subdec(2, 2);        // Level/Luck/Skill/Option
        $this->excellentOptions = $this->subdec(14, 2);   // Excellent Options

        // Skill Check
        if ($this->basicOptions >= 128) {
            $this->basicOptions -= 128;
            $this->hasSkill = true;
        }

        // Item Level
        $this->level = floor($this->basicOptions / 8);
        $this->basicOptions -= ($this->level * 8);

        // Fetch the item from the database. At this point we have all we need
        // and if it's not found it's not worth it to calculate the other options
        $this->applyItemModel();

        if ($this->get('level') === 0) {
            $this->showLevel = true;
        }

        // Luck Check
        if ($this->basicOptions >= 4) {
            $this->basicOptions -= 4;
            $this->hasLuck = true;
        }

        // Excellent Options
        if ($this->excellentOptions >= 128) {
            $this->excellentOptions -= 128;
        }

        if ($this->excellentOptions >= 64) {
            $this->excellentOptions -= 64;
            $this->basicOptions += 4;
        }

        $this->setExcellentOptions();

        // Jewel of Life Option
        $this->setJewelOfLifeOption();

        // Item Type
        $this->type = 1;

        // Item Image
        $this->setItemImage();
    }

    private function setItemIdAndGroup() {
        $id = $this->subdec(1, 1);
        $group = $this->subdec(0, 1);
        $excellentOptions = $this->subdec(14, 2);

        if ($group % 2 !== 0) {
            $group -= 1;
            $id += 16;
        }

        if ($excellentOptions >= 128) {
            $group += 16;
        }

        $group /= 2;

        $this->id = $id;
        $this->group = $group;
    }

    private function setExcellentOptions() {
        $this->excellentOptionsText = $this->calculateExcellentOptions($this->excellentOptions, [
            $this->get('op1'), $this->get('op2'),
            $this->get('op3'), $this->get('op4'),
            $this->get('op5'), $this->get('op6'),
        ], $this->excellentOptionsArray);
    }

    private function setJewelOfLifeOption() {
        $this->jewelOfLifeOption = $this->getJewelOfLifeOption($this->group, $this->basicOptions);
    }

    private function setItemImage() {
        // If the level is visible, that's not item changing it's image according to it
        $level = $this->showLevel ? 0 : $this->level;
        $this->image = $this->getItemImage($this->id, $this->group, $this->type, $level);
    }

    public function toJson() {
        $details = [
            'name' => $this->get('name'),
            'durability' => $this->durability,
            'dimensions' => [
                'x' => $this->get('x'),
                'y' => $this->get('y')
            ],
            'image' => $this->image
        ];

        if ($this->showLevel) {
            $details['level'] = $this->level;
        }

        if ($this->get('luck') !== 0) {
            $details['luck'] = $this->hasLuck;
        }

        if ($this->get('skill') !== 0) {
            $details['skill'] = $this->hasSkill ? [
                'name' => $this->get('skillName')
            ] : false;
        }

        if ($this->get('hasOption') !== 0) {
            $details['option'] = $this->jewelOfLifeOption;
        }

        if ($this->get('exctype') !== 0) {
            $details['excellent'] = $this->excellentOptionsText;
        }

        return $details;
    }
}
