<?php

namespace App\Models\Item;

use App\Traits\ItemMethods;
use App\Models\Item as Item;
use Illuminate\Support\Facades\DB;

abstract class BaseDecode {
    use ItemMethods;

    protected $hex;
    protected $hexLength = 32;
    protected $item;

    protected $id;
    protected $group;
    protected $durability;
    protected $ancientType;
    protected $serial;
    protected $basicOptions;
    protected $excellentOptions;
    protected $excellentOptionsArray = [
        false, false, false,
        false, false, false
    ];
    protected $type = 0;

    // Renderable values
    public $excellentOptionsText = [];
    public $jewelOfLifeOption = [];
    public $fenrirOptions = [];
    public $ancientSet;
    public $image;
    public $hasSkill = false;
    public $hasLuck = false;
    public $showLevel = false;
    public $level;

    public function __construct($hex) {
        if (substr($hex, 0, 2) === '0x') {
            $hex = substr($hex, 2);
        }

        if (strlen($hex) !== $this->hexLength) {
            throw new \Exception('The item cannot be processed.', 422);
        }

        $this->hex = $hex;
        $this->decode();
    }

    public function isExcellent() {
        return count($this->excellentOptionsText) !== 0;
    }

    public function isAncient() {
        return $this->ancientSet !== null;
    }

    public function isFenrir() {
        return $this->group === 13 && $this->id === 37;
    }

    protected function applyItemModel() {
        $this->item = new Item($this->id, $this->group, $this->level);
    }

    protected function get($key) {
        return $this->item->get($key);
    }

    abstract protected function decode();
}
