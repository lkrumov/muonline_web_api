<?php

namespace App\Models\Item;

use App\Traits\GenerateItemMethods;
use App\Models\Base as Model;
use App\Models\Item;
use App\Interfaces\IJsonable;

use Illuminate\Support\Facades\DB;

abstract class BaseGenerate implements IJsonable {
    use GenerateItemMethods;

    private $options = [];
    private $item = null;
    private $hex = null;

    public function __construct(array $options) {
        $this->item = new Item($options['id'], $options['group'], $options['level']);
        $this->options = $options;
    }

    public function getHex() {
        if ($this->hex === null) {
            $this->hex = $this->generateItemHex();
        }

        return $this->hex;
    }

    abstract protected function generateItemHex();

    public function toJson() {
        return [
            'name' => $this->get('name'),
            'hex' => $this->getHex(),
            'options' => $this->options
        ];
    }

    protected function get($key) {
        return $this->item->get($key);
    }

    protected function option($key) {
        if (!isset($this->options[$key])) {
            throw new \Exception('Required option ' . $key . ' not found!', 404);
        }

        return $this->options[$key];
    }

}
