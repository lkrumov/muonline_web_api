<?php

namespace App\Models\Item;

use App\Models\Item\BaseGenerate;
use App\Interfaces\IItemGenerator;

class Generate extends BaseGenerate {
    protected function generateItemHex() {
        // TODO: Add validations (e.g. does the item level can be changed)
        if ($this->get('level') !== 0 && $this->option('level') !== 0) {
            throw new \Exception('You cannot set level on the specific item.', 428);
        }

        $hex = [];
        $hex[] = $this->generateId($this->get('id'));
        $hex[] = $this->generateBasicOptions(
            $this->option('level'),
            $this->option('option'),
            $this->option('skill'),
            $this->option('luck')
        );
        $hex[] = $this->generateDurabillity($this->get('durabillity'));
        $hex[] = $this->generateSerial();
        $hex[] = $this->generateExcellentOptions(
            $this->option('excellentOptions'),
            $this->option('option')
        );
        $hex[] = $this->generateAncientOption($this->option('ancient'));
        $hex[] = $this->generateGroup($this->get('type'));
        $hex[] = $this->generateRefineryOptions($this->option('refinery'));
        $hex[] = $this->generateHarmonyOption($this->option('harmony'));
        $hex[] = $this->generateSocketOptions($this->option('socket'));

        return join('', $hex);
    }
}
