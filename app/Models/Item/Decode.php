<?php

namespace App\Models\Item;

use App\Interfaces\IDecodable;
use App\Interfaces\IJsonable;

class Decode extends BaseDecode implements IJsonable {
    protected $refineryOptions;
    protected $harmonyOption;
    protected $socketOptions;

    // Renderable values
    public $refineryOptionsText = [];
    public $socketOptionsText = [];
    public $harmonyOptionText = [];

    protected function decode() {
        // Item's basic information
        $this->id = $this->subdec(0, 2);                 // ID
        $this->group = $this->subdec(18, 1);             // Group
        $this->durability = $this->subdec(4, 2);         // Durability
        $this->ancientType = $this->subdec(16, 2);       // Ancient Type
        $this->serial = $this->substr(6, 8);             // Serial Number

        // Item's options
        $this->basicOptions = $this->subdec(2, 2);        // Level/Luck/Skill/Option
        $this->excellentOptions = $this->subdec(14, 2);   // Excellent Options
        $this->refineryOptions = $this->substr(19, 1);    // Refinery Options
        $this->harmonyOption = $this->substr(20, 2);      // Harmony Options
        $this->socketOptions = $this->substr(22, 10);     // Socket Options

        // Skill Check
        if ($this->basicOptions >= 128) {
            $this->basicOptions -= 128;
            $this->hasSkill = true;
        }

        // Item Level
        $this->level = floor($this->basicOptions / 8);
        $this->basicOptions -= ($this->level * 8);

        // Fetch the item from the database. At this point we have all we need
        // and if it's not found it's not worth it to calculate the other options
        $this->applyItemModel();

        if ($this->get('level') === 0) {
            $this->showLevel = true;
        }

        // Luck Check
        if ($this->basicOptions >= 4) {
            $this->basicOptions -= 4;
            $this->hasLuck = true;
        }

        // Excellent Options
        if ($this->excellentOptions >= 64) {
            $this->excellentOptions -= 64;
            $this->basicOptions += 4;
        }

        $this->setExcellentOptions();

        // If we have a Fenrir we don't need to calculate all of the options
        if ($this->isFenrir()) {
            $this->setFenrirOptions();
            return;
        }

        // Jewel of Life Option
        $this->setJewelOfLifeOption();

        // Refinery Options
        if ($this->get('refinery') !== 0 && $this->refineryOptions === '8') {
            $this->setRefineryOptions();
        }

        // Socket Options
        if ($this->get('socket') !== 0) {
            $this->setSocketOptions();
        }

        // Jewel of Harmony Options
        if ($this->get('harmony') !== 0 && $this->harmonyOption !== '00') {
            $this->setHarmonyOption();
        }

        // Ancient Sets
        if ($this->get('ancient') !== 0 && $this->ancientType !== 0) {
            if ($this->get('set1') !== 0 && $this->ancientType < 10) {
                $this->ancientSet = $this->get('set1');
            } else if ($this->get('set2') !== 0) {
                $this->ancientSet = $this->get('set2');
            }
        }

        // Item Type
        if ($this->isExcellent() && $this->get('type') !== 12) {
            $this->type = 1;
        } else if ($this->isAncient()) {
            $this->type = 2;
        }

        // Item Image
        $this->setItemImage();
    }

    private function setExcellentOptions() {
        $this->excellentOptionsText = $this->calculateExcellentOptions($this->excellentOptions, [
            $this->get('op1'), $this->get('op2'),
            $this->get('op3'), $this->get('op4'),
            $this->get('op5'), $this->get('op6'),
        ], $this->excellentOptionsArray);
    }

    private function setRefineryOptions() {
        $this->refineryOptionsText = $this->getRefineryOptions($this->group);
    }

    private function setSocketOptions() {
        $this->socketOptionsText = $this->getSockets($this->socketOptions);
    }

    private function setHarmonyOption() {
        $this->harmonyOptionText = $this->getHarmonyOption($this->harmonyOption, $this->get('harmony'));
    }

    private function setFenrirOptions() {
        $this->fenrirOptions = $this->getFenrirOptions($this->excellentOptionsArray);
    }

    private function setJewelOfLifeOption() {
        $this->jewelOfLifeOption = $this->getJewelOfLifeOption($this->group, $this->basicOptions);
    }

    private function setItemImage() {
        // If the level is visible, that's not item changing it's image according to it
        $level = $this->showLevel ? 0 : $this->level;
        $this->image = $this->getItemImage($this->id, $this->group, $this->type, $level);
    }

    public function toJson() {
        $details = [
            'name' => $this->get('name'),
            'durability' => $this->durability,
            'dimensions' => [
                'x' => $this->get('x'),
                'y' => $this->get('y')
            ],
            'image' => $this->image
        ];

        if ($this->isFenrir()) {
            $details = array_merge($details, $this->fenrirOptions);
        } else {
            if ($this->showLevel) {
                $details['level'] = $this->level;
            }

            if ($this->get('luck') !== 0) {
                $details['luck'] = $this->hasLuck;
            }

            if ($this->get('skill') !== 0) {
                $details['skill'] = $this->hasSkill ? [
                    'name' => $this->get('skillName')
                ] : false;
            }

            if ($this->get('hasOption') !== 0) {
                $details['option'] = $this->jewelOfLifeOption;
            }

            if ($this->get('exctype') !== 0) {
                $details['excellent'] = $this->excellentOptionsText;
            }

            if ($this->get('ancient') !== 0 && $this->ancientType > 0) {
                $details['ancient'] = [
                    'set' => $this->ancientSet,
                    'type' => $this->ancientType
                ];
            }

            if ($this->get('refinery') !== 0) {
                $details['refinery'] = $this->refineryOptionsText;
            }

            if ($this->get('harmony') !== 0) {
                $details['harmony'] = $this->harmonyOptionText;
            }

            if ($this->get('socket') !== 0) {
                $details['sockets'] = $this->socketOptionsText;
            }
        }

        return $details;
    }
}
