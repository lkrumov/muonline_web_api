<?php

namespace App\Models;

use Illuminate\Database\Query\Builder as Builder;
use Illuminate\Support\Facades\DB;

class Base {
    protected $data;
    protected $source;
    protected $varbinaryFields = [];

    /**
     * Get model property
     *
     * @param string $key
     * @return mixed value
     */
    public function get($key) {
        $value = $this->getData($key);

        // MuOnline's database is sometimes using characters to store integers
        // We're trying to parse the integer value and if it's typeless matching
        // the old one we're returning integer variable
        return $this->tryParse($value, 'int');
    }

    /**
     * Get all of the model's properties
     *
     * @return array model's data
     */
    public function getAll() {
        $data = (array) $this->data;

        foreach ($data as $key => $value) {
            $data[$key] = $this->tryParse($value, 'int');
        }

        return $data;
    }

    /**
     * Get model properties
     *
     * @param string $keys
     * @return array $data
     */
    public function getMany($keys) {
        $data = [];

        foreach ($keys as $key) {
            $data[$key] = $this->get($key);
        }

        return $data;
    }

    /**
     * Set model property
     *
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value) {
        if (strpos($value, '-') === 0) {
            $value = $this->get($key) - abs(intval($value));
        } else if (strpos($value, '+') === 0) {
            $value = $this->get($key) + abs(intval($value));
        }

        $this->data->{$key} = $value;
    }

    /**
     * Update multiple properties
     *
     * @param array $properties Kay-Value paired properties to be updated
     */
    public function setMany(array $properties) {
        foreach ($properties as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * Save the model to the database
     *
     * @return Builder instance
     */
    public function save() {
        $fields = get_object_vars($this->data);

        // A little trick to save varbinary fields :)
        foreach ($this->varbinaryFields as $field) {
            $fields[$field] = DB::raw('0x' . $this->get($field));
        }

        $update = $this->source->update($fields);

        return $update;
    }

    /**
     * Set the model data source (Illuminate Builder only for now)
     *
     * @param Builder $source
     */
    protected function setSource(Builder $source) {
        $this->source = $source;
    }

    /**
     * Overwrite model's data
     *
     * @param stdClass $data
     */
    protected function setData(\stdClass $data) {
        $this->data = $data;
    }

    /**
     * Get data by key
     *
     * @param string $key
     * @return mixed value
     */
    protected function getData($key) {
        if(!isset($this->data->{$key})) {
            return null;
        }

        return $this->data->{$key};
    }

    /**
     * Try to parse given value to given type
     *
     * @param mixed $value
     * @param string $type data type
     * @return mixed parsed value or the same value
     */
    protected function tryParse($value, $type) {
        $parsed = $value;

        // TODO: Implement more castings if needed
        // FIXME: Run the value through all casting options
        switch ($type) {
            case 'int':
            case 'integer':
                $parsed = (int) $value;
                break;
            case 'bool':
            case 'boolean':
                if(in_array(strtolower($parsed) , ['true', 'false'])) {
                    return (strtolower($parsed) === 'true' ?  true : false);
                }
                break;
        }

        return (string) $parsed == $value ? $parsed : $value;
    }

}
