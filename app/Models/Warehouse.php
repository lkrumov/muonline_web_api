<?php

namespace App\Models;

use App\Models\ItemStorage;

use Illuminate\Support\Facades\DB;

class Warehouse extends ItemStorage {
    protected $varbinaryFields = [
        'Items'
    ];

    protected $itemColumn = 'Items';

    public function __construct($username) {
        $warehouse = DB::table('warehouse')
                        ->select('Items')
                        ->where('AccountID', $username);

        if ($warehouse->count() !== 1) {
            // TODO: Insert warehouse if it doesn't exists?
            throw new \Exception('Warehouse not found.', 404);
        }

        $this->setSource($warehouse);
        $this->setData($warehouse->first());
        $this->configure();
    }

    private function configure() {
        // We know for sure that the warehouse has 120 slots, so if we divide it's
        // length by 120, we will get the item hex length - no need to get server's version
        $this->setHexLength(strlen($this->get('Items')) / 120);
        $this->setSlotsCount(120);
        $this->setSlotsWidth(8);
    }
}
