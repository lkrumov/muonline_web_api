<?php

namespace App\Models;

use App\Models\Character;
use App\Models\Base as Model;

use Illuminate\Support\Facades\DB;

class Guild extends Model {
    protected $varbinaryFields = [
        'G_Mark'
    ];

    public function __construct($name) {
        $members = DB::raw('(Select count(*) from GuildMember Where G_Name=Guild.G_Name) as Members');
        $guild = DB::table('Guild')
                    ->select([
                        'Guild.*',
                        $members
                    ])->where('G_Name', $name);

        if ($guild->count() !== 1) {
            throw new \Exception('Guild not found.', 404);
        }

        $this->setSource($guild);
        $this->setData($guild->first());
    }

    public function getMembers() {
        $members = DB::table('GuildMember')
                    ->select('Name', 'G_Status', 'G_Level')
                    ->where('G_Name', $this->get('G_Name'))
                    ->get();
        $membersArray = [];

        foreach ($members as $member) {
            $memberInfo = (new Character($member->Name))->getMany([
                'Name', 'cLevel', 'Resets', 'Class'
            ]);

            $memberInfo['G_Status'] = (int) $member->G_Status;
            $memberInfo['G_level'] = (int) $member->G_Level;
            $membersArray[] = $memberInfo;
        }

        return $membersArray;
    }
}
