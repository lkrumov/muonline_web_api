<?php

namespace App\Models;

use App\Traits\ItemMethods;
use App\Models\Base as Model;
use Illuminate\Support\Facades\DB;

class Item extends Model {
    public function __construct($id, $group, $level) {
        if (!isset($id) || !isset($group) || !isset($level)) {
            throw new \Exception('Invalid options for Item to construct.', 400);
        }

        $item = $this->fetchItem($id, $group, $level);
        $this->setData($item);
    }

    private function fetchItem($id, $group, $level) {
        if ($group <= 11) {
            $item = $this->getNormalItem($id, $group);
        } else {
            $item = $this->getLevelItem($id, $group, $level);

            if ($item->count() !== 1) {
                $item = $this->getNormalItem($id, $group);
            }
        }

        if ($item->count() === 1) {
            return $item->first();
        }

        throw new \Exception('Item not found', 404);
    }

    private function getLevelItem($id, $group, $level) {
        $conditions = [
            ['DPWebShop_Items.id', '=', $id],
            ['DPWebShop_Items.type', '=', $group],
            ['DPWebShop_Items.level', '=', $level]
        ];

        $item = DB::table('DPWebShop_Items')
                    ->select([
                        '*',
                        'DPWebShop_Items.option as hasOption'
                    ])
                    ->leftJoin(
                        'DPWebShop_ExcellentTypes',
                        'DPWebShop_ExcellentTypes.type', '=',
                        'DPWebShop_Items.exctype'
                    )
                    ->leftJoin(
                        'DPWebShop_ItemSkill',
                        'DPWebShop_ItemSkill.skill', '=',
                        'DPWebShop_Items.skill'
                    )
                    ->where($conditions);

        return $item;
    }

    private function getNormalItem($id, $group) {
        $conditions = [
            ['DPWebShop_Items.id', '=', $id],
            ['DPWebShop_Items.type', '=', $group]
        ];

        $item = DB::table('DPWebShop_Items')
                    ->select([
                        '*',
                        'DPWebShop_Items.option as hasOption'
                    ])
                    ->leftJoin(
                        'DPWebShop_ExcellentTypes',
                        'DPWebShop_ExcellentTypes.type', '=',
                        'DPWebShop_Items.exctype'
                    )
                    ->leftJoin(
                        'DPWebShop_ItemSkill',
                        'DPWebShop_ItemSkill.skill', '=',
                        'DPWebShop_Items.skill'
                    )
                    ->where($conditions);

        return $item;
    }
}
