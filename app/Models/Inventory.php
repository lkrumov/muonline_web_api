<?php

namespace App\Models;

use App\Models\ItemStorage;

use Illuminate\Support\Facades\DB;

class Inventory extends ItemStorage {
    // The first 12 slots in the Inventory are his equipment,
    // items can't be inserted there.
    protected $defaultOffset = 12;
    protected $varbinaryFields = [
        'Inventory'
    ];
    protected $itemColumn = 'Inventory';

    public function __construct($name) {
        $inventory = DB::table('Character')
                        ->select('Inventory')
                        ->where('Name', $name);

        if ($inventory->count() !== 1) {
            // TODO: Insert warehouse if it doesn't exists?
            throw new \Exception('Character inventory not found.', 404);
        }

        $this->setSource($inventory);
        $this->setData($inventory->first());
        $this->configure();
    }

    private function configure() {
        // We know for sure that the Inventory has 108 slots, so if we divide it's
        // length by 108, we will get the item hex length - no need to get server's version
        $this->setHexLength(strlen($this->get('Inventory')) / 108);
        $this->setSlotsCount(108);
        $this->setSlotsWidth(8);
    }
}
