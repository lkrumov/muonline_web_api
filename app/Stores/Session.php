<?php

namespace App\Stores;

class Session {
    public static function set($key, $value) {
        return $_SESSION[$key] = $value;
    }

    public static function get($key) {
        return self::has($key) ? $_SESSION[$key] : null;
    }

    public static function has($key) {
        return isset($_SESSION[$key]);
    }

    public static function flush() {
        session_destroy();
        $_SESSION = [];
    }
}
