<?php

namespace App\Validators;

class Username extends BaseValidator {
    public function condition() {
        return [
            'matches' => '^[A-Za-z0-9_\-.]{4,10}$',
            'minlength' => 4,
            'maxlength' => 10
        ];
    }
}
