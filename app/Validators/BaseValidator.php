<?php

// TODO: Decide if Custom validators will be used or the Lumen's buil-in ones

namespace App\Validators;

use Log;
use Exception;
use ReflectionClass;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as Controller;
use App\Exceptions\SecurityException as SecurityException;

abstract class BaseValidator {
    protected $value;

    public function __construct($value) {
        $this->value = $value;

        try {
            $this->validate();
        } catch(SecurityException $e) {

        }
    }

    public function __toString() {
        return $this->value;
    }

    private function validate() {
        $condition = $this->condition();
        $validator = (new ReflectionClass($this))->getShortName();

        if (isset($condition['matches'])) {
            $pattern = $condition['matches'];

            if (!preg_match("/{$pattern}/", $this->value)) {
                throw new SecurityException("{$validator} failed to validate the entered input!", 400);
            }
        }

        if (isset($condition['minlength'])) {
            $min = $condition['minlength'];

            if (strlen($this->value) < $min) {
                throw new Exception("{$validator}'s length should at least {$min} characters!", 400);
            }
        }

        if (isset($condition['maxlength'])) {
            $max = $condition['maxlength'];

            if (strlen($this->value) > $max) {
                throw new Exception("{$validator}'s length should not exceep {$max} characters!", 400);
            }
        }
    }

    abstract function condition();
}
