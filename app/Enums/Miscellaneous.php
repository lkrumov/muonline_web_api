<?php

namespace App\Enums;

abstract class Miscellaneous {
    const NO_EFFECT = 'NO EFFECT';
    const FENRIR_ILLUSION = 'GOLDEN';
    const FENRIR_DEFENSE = 'DEFENSE';
    const FENRIR_DEMAGE = 'DEMAGE';
}
