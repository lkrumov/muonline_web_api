<?php

namespace App\Enums;

abstract class CharacterClass {
    // Dark Wizard, Soul Master & Grand Master
    const DW = 0;
    const SM = 1;
    const GM = [2, 3];
    // Dark Knight, Blade Knight & Blade Master
    const DK = 16;
    const BK = 17;
    const BM = [18, 19];
    // Elf, Muse Elf & High Elf
    const ELF = 32;
    const ME = 34;
    const HE = [35, 36];
    // Magic Gladiator & Duel Master
    const MG = 48;
    const DM = [49, 50];
    // Dark Lord & Lord Emperor
    const DL = 64;
    const LE = [65, 66];
    // Summoner, Bloody Summoner & Dimension Master
    const SU = 80;
    const BS = 81;
    const DIM = [82, 83];

    public static function isWizard($class) {
        return self::isDarkWizard($class) || self::isSoulMaster($class) || self::isGrandMaster($class);
    }

    public static function isDarkWizard($class) {
        return $class === self::DW;
    }

    public static function isSoulMaster($class) {
        return $class === self::SM;
    }

    public static function isGrandMaster($class) {
        return in_array($class, self::GM);
    }

    public static function isKnight($class) {
        return self::isDarkKnight($class) || self::isBladeKnight($class) || self::isBladeMaster($class);
    }

    public static function isDarkKnight($class) {
        return $class === self::DK;
    }

    public static function isBladeKnight($class) {
        return $class === self::BK;
    }

    public static function isBladeMaster($class) {
        return in_array($class, self::BM);
    }

    public static function isElf($class) {
        return self::isBaseElf($class) || self::isMuseElf($class) || self::isHighElf($class);
    }

    public static function isBaseElf($class) {
        return $class === self::ELF;
    }

    public static function isMuseElf($class) {
        return $class === self::ME;
    }

    public static function isHighElf($class) {
        return in_array($class, self::HE);
    }

    public static function isGladiator($class) {
        return self::isMagicGladiator($class) || self::isDuelMaster($class);
    }

    public static function isMagicGladiator($class) {
        return $class === self::MG;
    }

    public static function isDuelMaster($class) {
        return in_array($class, self::DM);
    }

    public static function isLord($class) {
        return self::isDarkLord($class) || self::isLordEmperor($class);
    }

    public static function isDarkLord($class) {
        return $class === self::DL;
    }

    public static function isLordEmperor($class) {
        return in_array($class, self::LE);
    }

    public static function isSummoner($class) {
        return self::isBaseSummoner($class) || self::isBloodySummoner($class) || self::isDimensionMaster($class);
    }

    public static function isBaseSummoner($class) {
        return $class === self::SU;
    }

    public static function isBloodySummoner($class) {
        return $class === self::BS;
    }

    public static function isDimensionMaster($class) {
        return in_array($class, self::DIM);
    }

}
