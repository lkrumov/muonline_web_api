<?php

namespace App\Exceptions;

use Log;
use Exception;
use ReflectionClass;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $exception = (new ReflectionClass($e))->getShortName();
        $log = [
            'path' => $request->path(),
            'query' => $request->query(),
            'client' => $request->ip(),
            'input' => $request->all(),
            'exception' => $exception
        ];

        if (!preg_match("/^([A-Za-z0-9_\-]+\/?)+$/", $request->path()) ||
            $request->getQueryString() && !preg_match("/^([A-Za-z0-9_\-]+\/?)+$/", $request->getQueryString())) {
            Log::alert("[SECURITY] A possible SQL Injection has been detected. Please review.", $log);
        } else {
            Log::notice("An {$exception} has occured", $log);
        }

        return parent::render($request, $e);
    }
}
