<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Stores\Session;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate {
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $session = Session::get(config('session.name'));
        $unauthorized = response('Unauthorized.', 401);

        if ($session === null) {
            return $unauthorized;
        } else {
            $route = $request->route();
            $username = array_get($route[2], 'username', null);
            $character = array_get($route[2], 'character', null);

            if ($username !== null && $username !== $session) {
                return $unauthorized;
            }

            if ($character !== null) {
                $user = new User($session);

                if (!$user->hasCharacter($character)) {
                    return $unauthorized;
                }
            }
        }

        return $next($request);
    }
}
