<?php

namespace App\Http\Controllers\User;

use Log;
use Illuminate\Http\Request;

use App\Models\User as UserModel;

class User {

    public function index($name) {
        $user = new UserModel($name);

        return $user->getMany([
            'memb___id', 'ctl1_code', 'bloc_code', 'GameID1',
            'GameID2', 'GameID3', 'GameID4', 'GameID5',
            'GameIDC', 'ConnectStat', 'ServerName'
        ]);
    }
}
