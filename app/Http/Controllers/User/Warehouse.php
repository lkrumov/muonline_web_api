<?php

namespace App\Http\Controllers\User;

use App\Models\Warehouse as WarehouseModel;

use Illuminate\Http\Request;

class Warehouse {

    public function index($username) {
        $warehouse = new WarehouseModel($username);
        return $warehouse->getItems();
    }
}
