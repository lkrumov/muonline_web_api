<?php

namespace App\Http\Controllers\Character;

use App\Models\Inventory as InventoryModel;

class Inventory {

    public function index($name) {
        $inventory = new InventoryModel($name);
        return $inventory->getItems();
    }
}
