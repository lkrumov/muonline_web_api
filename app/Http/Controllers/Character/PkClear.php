<?php

namespace App\Http\Controllers\Character;

use Log;

use App\Http\Controllers\Base as Controller;
use App\Models\Character as Character;

class PkClear extends Controller {

    public function index($name) {
        $character = new Character($name);

        if ($character->get('PkLevel') === 3) {
            $this->error('You don\'t have a killer status.');
        }

        $cost = (!config('server.pkclear.perkill') ?
                    1 : ($character->get('PkTime') === '0' ? 1 : $character->get('PkTime')))
                * config('server.pkclear.cost');

        if ($character->get('Money') < $cost) {
            $this->error('You don\'t have enough Zen for the PK clear.');
        }

        $this->bailValidation();

        $character->setMany([
            'PkLevel' => 3,
            'PkTime' => 0,
            'Money' => "-$cost"
        ]);

        if ($character->save()) {
            Log::info("Character ${name} pk cleared");
            return response([
                'message' => 'Pk cleared.',
                'PkLevel' => 3,
                'PkTime' => 0
            ]);
        }
    }
}
