<?php

namespace App\Http\Controllers\Character;

use Log;
use Illuminate\Http\Request;

use App\Http\Controllers\Base as Controller;
use App\Models\Character as Character;

class AddStats extends Controller {

    public function index(Request $request, $name) {
        $character = new Character($name);

        $this->validate($request, [
            'strength' => 'bail|required|integer',
            'dexterity' => 'bail|required|integer',
            'vitality' => 'bail|required|integer',
            'energy' => 'bail|required|integer',
            'leadership' => 'bail|integer'
        ]);

        $strength = (int) $request->input('strength');
        $dexterity = (int) $request->input('dexterity');
        $vitality = (int) $request->input('vitality');
        $energy = (int) $request->input('energy');
        $leadership = (int) $request->input('leadership');
        $totalPointsUsed = $strength + $dexterity + $vitality + $energy + $leadership;

        if (!$character->isLeadershipEligible() && $leadership !== 0) {
            $this->error('You can\'t add leadership points to non-lord character.');
        }

        if ($character->get('LevelUpPoint') < $totalPointsUsed) {
            $this->error('You don\'t have enough level up points to add.');
        }

        if ($character->get('Money') < config('server.addstats.cost')) {
            $this->error('You don\'t have enough Zen to add stats.');
        }

        // If the validations above fail, don't bother to do the calculations below
        $this->bailValidation();

        if (($character->get('Strength') + $strength) > config('server.maxstats') ||
            ($character->get('Dexterity') + $dexterity) > config('server.maxstats') ||
            ($character->get('Vitality') + $vitality) > config('server.maxstats') ||
            ($character->get('Energy') + $energy) > config('server.maxstats') ||
            ($character->get('Leadership') + $leadership) > config('server.maxstats')
        ) {
            $this->error('You have exceeded the server maximum stat.');
        }

        $this->bailValidation();

        $character->setMany([
            'Strength' => "+$strength",
            'Dexterity' => "+$dexterity",
            'Vitality' => "+$vitality",
            'Energy' => "+$energy",
            'Leadership' => "+$leadership",
            'LevelUpPoint' => "-$totalPointsUsed"
        ]);

        if ($character->save()) {
            Log::info("Character ${name} added stats.");
            return response([
                'message' => 'Stats added.',
                'Strength' => $character->get('Strength'),
                'Dexterity' => $character->get('Dexterity'),
                'Vitality' => $character->get('Vitality'),
                'Energy' => $character->get('Energy'),
                'Leadership' => $character->get('Leadership'),
                'LevelUpPoint' => $character->get('LevelUpPoint')
            ]);
        }
    }
}
