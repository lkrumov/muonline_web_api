<?php

namespace App\Http\Controllers\Character;

use Log;

use App\Http\Controllers\Base as Controller;
use App\Models\Character as Character;

class Reset extends Controller {

    public function index($name) {
        $character = new Character($name);

        if ($character->get('Resets') >= config('server.reset.max')) {
            $this->error('You have reached the Resets limit.');
        }

        if ($character->get('cLevel') < config('server.reset.level')) {
            $this->error('You are below the required level for reset.');
        }

        if ($character->get('Money') < config('server.reset.cost')) {
            $this->error('You don\'t have enough Zen for reset.');
        }

        $this->bailValidation();

        $character->setMany([
            'Resets' => '+1',
            'cLevel' => 1,
            'Experience' => 0,
            'MapNumber' => 0,
            'MapPosX' => 125,
            'MapPosY' => 128,
            'LevelUpPoint' => '+' . config('server.reset.points'),
            'Money' => '-' . config('server.reset.cost')
        ]);

        if ($character->save()) {
            Log::info("Character ${name} reseted");
            return response([
                'message' => 'Character reseted.',
                'Resets' => ($character->get('Resets') + 1)
            ]);
        }
    }
}
