<?php

namespace App\Http\Controllers\Character;

use App\Models\Character as CharacterModel;

class Character {

    public function index($name) {
        $character = new CharacterModel($name);

        return $character->getMany([
            'Name', 'cLevel', 'LevelUpPoint', 'Experience',
            'Strength', 'Dexterity', 'Vitality', 'Vitality',
            'Energy', 'Inventory', 'MagicList', 'MapNumber',
            'MapPosX', 'MapPosY', 'PkCount', 'PkLevel',
            'PkTime', 'CtlCode', 'Leadership', 'Resets', 'Connected'
        ]);
    }
}
