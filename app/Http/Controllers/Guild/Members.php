<?php

namespace App\Http\Controllers\Guild;

use App\Models\Guild;

class Members {

    public function index($name) {
        $guild = new Guild($name);
        return $guild->getMembers();
    }
}
