<?php

namespace App\Http\Controllers\Guild;

use App\Models\Guild as GuildModel;

class Guild {

    public function index($name) {
        $guild = new GuildModel($name);

        return $guild->getMany([
            'G_Name', 'G_Master', 'G_Mark', 'G_Score', 'G_Notice', 'Members'
        ]);
    }
}
