<?php

namespace App\Http\Controllers\Auth;

use App\Stores\Session;
use App\Http\Controllers\Base as Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class Login extends Controller {

    public function index(Request $request) {
        // Set default error messages
        $message = 'Invalid username and/or password';
        $success = false;

        $this->validate($request, [
            'username' => 'bail|required|min:4|max:10',
            'password' => 'bail|required'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');

        if (config('server.md5')) {
            $password = '[dbo].[fn_md5](\'' . $password . '\', \'' . $username . '\')';
        }

        $user = DB::table('MEMB_INFO')
                    ->where([
                        'memb___id' => $username,
                        'memb__pwd' => $password,
                    ]);

        if ($user->count() === 1) {
            Session::set(config('session.name'), $username);
            $message = 'You\'ve been logged in successfully.';
            $success = true;
        }

        return response([
            'message' => $message,
            'success' => $success
        ], $success ? 200 : 404);
    }
}
