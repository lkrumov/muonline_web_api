<?php

namespace App\Http\Controllers\Auth;

use App\Stores\Session;
use App\Http\Controllers\Base as Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class Register extends Controller {

    public function index(Request $request) {
        // Set default error messages
        $message = 'Invalid username and/or password';
        $success = false;

        $this->validate($request, [
            'username' => 'bail|required|min:4|max:10',
            'password' => 'bail|required',
            'confirm-password' => 'bail|required',
            'secret-question' => 'bail|required|min:4|max:10',
            'secret-answer' => 'bail|required|min:4|max:10'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');
        $confirmPassword = $request->input('confirm-password');
        $secretQuestion = $request->input('secret-question');
        $secretAnswer = $request->input('secret-answer');

        if (config('server.md5')) {
            $password = '[dbo].[fn_md5](\'' . $password . '\', \'' . $username . '\')';
        }

        if ($register) {
            Session::set(config('session.name'), $username);
            $message = 'You\'ve been registered successfully.';
            $success = true;
        }

        return response([
            'message' => $message,
            'success' => $success
        ], $success ? 200 : 404);
    }
}
