<?php

session_set_cookie_params(86400, '/', '', false, true);
session_start();

$app = require __DIR__.'/../bootstrap/app.php';
$app->run();
